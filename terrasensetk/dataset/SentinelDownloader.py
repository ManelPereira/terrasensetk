import geopandas as gpd
import pandas as pd
import os

from . import Downloader
from sentinelhub import DataCollection


class SentinelDownloader(Downloader):
    """
    Downloader class made for Sentinel-2 satelite

    """
    # Collection name of Sentinel-2 to be used by the sentinel hub
    collection = DataCollection.SENTINEL2_L2A
    # Available scripts for the satelite
    indices = tuple([s.split('.')[0] for s in os.listdir('./terrasensetk/satelite-scripts/sentinel-2/')])
    bands = ('B01', 'B02', 'B03', 'B04', 'B05', 'B06', 'B07', 'B08', 'B8A', 'B09', 'B11', 'B12')
    name = 'sentinel-2'

    def __init__(self, config, shapefile):
        super().__init__(config, shapefile=shapefile)
        

    def get_groundtruth(self, crop = None):
        """
        Returns the groundtruth example in the LUCAS Copernicus dataset
        
        Args:
            crop: which crop to filter by possible values: 
                ['Grassland without tree/shrub cover',
                'Shrubland without tree cover', 'Peatbogs', 'Inland marshes',
                'Spontaneously vegetated surfaces', 'Rape and turnip rape',
                'Barley', 'Spruce dominated mixed woodland',
                'Grassland with sparse tree/shrub cover', 'Broadleaved woodland',
                'Temporary grasslands', 'Spruce dominated coniferous woodland',
                'Oats', 'Clovers', 'Sunflower',
                'Pine dominated coniferous woodland', 'Other mixed woodland',
                'Common wheat', 'Other coniferous woodland',
                'Pine dominated mixed woodland',
                'Shrubland with sparse tree cover', 'Sugar beet', 'Lucerne', 'Rye',
                'Potatoes', 'Maize', 'Triticale',
                'Other fibre and oleaginous crops', 'Other root crops',
                'Apple fruit', 'Mixed cereals for fodder',
                'Other artificial areas', 'Durum wheat', 'Dry pulses', 'Vineyards',
                'Other bare soil', 'Olive groves', 'Nurseries', 'Oranges',
                'Tomatoes', 'Other leguminous and mixtures for fodder', 'Soya',
                'Other fruit trees and berries', 'Inland fresh running water',
                'Nuts trees', 'Pear fruit', 'Other non-permanent industrial crops',
                'Permanent industrial crops', 'Non built-up linear features',
                'Sand', 'Cotton', 'Cherry fruit', 'Other fresh vegetables',
                'Non built-up area features', 'Lichens and moss',
                'Rocks and stones', 'Other cereals', 'Strawberries',
                'Floriculture and ornamental plants', 'Other citrus fruit', 'Rice',
                'Buildings with 1 to 3 floors', 'Tobacco']
        Returns:
            GeoDataFrame: Contains the groundtruth data within the given region
        """
        #if self._groundtruth_points is not None:
        #    return self._groundtruth_points
        #groundtruth_gdf = pd.read_pickle(self.get_lucas_copernicus_path(),compression='bz2')
        #
        #if crop is not None:
        #    groundtruth_gdf = groundtruth_gdf[groundtruth_gdf.LC1_LABEL == crop]
        #
        #self._groundtruth_points = gpd.sjoin(groundtruth_gdf,self.dataset,predicate="within", how="inner").drop("index_right", axis="columns")
        #return self._groundtruth_points
        return self.dataset