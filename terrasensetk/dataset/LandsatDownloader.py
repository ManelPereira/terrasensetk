import os

from sentinelhub import DataCollection
from . import Downloader


class LandsatDownloader(Downloader):
    """
    Downloader class made for Landsat 4-5 TM satelite

    """
    # Collection name of Landsat 4-5 TM L2 to be used by the sentinel hub
    collection = DataCollection.LANDSAT_TM_L2
    # Available scripts for the satelite
    indices = tuple([s.split('.')[0] for s in os.listdir('./terrasensetk/satelite-scripts/landsat-4-5-TM/')])
    bands = ('B01', 'B02', 'B03', 'B04', 'B05', 'B06', 'B07')
    name = 'landsat-4-5-TM'

    def __init__(self, config, shapefile):
        super().__init__(config, shapefile=shapefile)


    def get_groundtruth(self, crop=None):
        return self.dataset