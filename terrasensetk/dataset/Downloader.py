import compress_pickle as cpickle
import matplotlib.pyplot as plt
import sentinelhub as sh
import geopandas as gpd
import pandas as pd
import numpy as np
import datetime
import shapely
import math
import os

from sentinelhub import BBox, DataCollection,CustomGridSplitter
from sentinelhub.areas import CustomGridSplitter

from ..utils.eotasks import CountValid, EuclideanNorm, SentinelHubValidData, AddValidDataMaskTask
from ..utils.utils import get_lucas_copernicus_path, get_time_interval

from shapely.geometry import Polygon

from eolearn.io import SentinelHubInputTask
from eolearn.core import EONode, EOWorkflow, FeatureType, OverwritePermission, \
      SaveTask, EOExecutor, MergeFeatureTask, AddFeatureTask
from eolearn.geometry import VectorToRasterTask


class Downloader:
    """
    Class to download a dataset based on Lucas Copernicus
    
    """

    def __init__(self,shapefile = None,bands = None, country = None, continent = None, config=None):
        
        self._init_classvars()
        if(shapefile is not None):
            self.dataset = gpd.read_file(shapefile)
        else:
            self._world = gpd.read_file(gpd.datasets.get_path('naturalearth_lowres'))
            if(country is not None):
                self.dataset = self._world[self._world.name==country]
            elif(continent is not None):
                self.dataset = self._world[self._world.continent==continent].dissolve(by="continent")
            else:
                ValueError("Either the shapefile or country must be provided")

        self._bands = bands
        #self._eopatch_size = eopatch_size
        self.dataset = self.dataset.to_crs(sh.CRS.WGS84.pyproj_crs())

    
    @classmethod
    def from_pickle(self,file):
        """Loads the pickled Downloader object

        Returns:
            Downloader: An instance that was saved in the pickle file
        """
        with open(file,'rb') as f:
            instance = cpickle.load(f,compression="bz2")
            return instance


        
    def to_pickle(self,file):
        """ Pickles the current Downloader object that can be loaded by the ´from_pickle´ function
        """
        with open(file,'wb') as f:
            cpickle.dump(self,f,compression="bz2")


    def get_groundtruth(self,crop = None):
        """Returns the groundtruth example in the LUCAS Copernicus dataset
        Args:
            crop: which crop to filter by possible values: 
                ['Grassland without tree/shrub cover',
                'Shrubland without tree cover', 'Peatbogs', 'Inland marshes',
                'Spontaneously vegetated surfaces', 'Rape and turnip rape',
                'Barley', 'Spruce dominated mixed woodland',
                'Grassland with sparse tree/shrub cover', 'Broadleaved woodland',
                'Temporary grasslands', 'Spruce dominated coniferous woodland',
                'Oats', 'Clovers', 'Sunflower',
                'Pine dominated coniferous woodland', 'Other mixed woodland',
                'Common wheat', 'Other coniferous woodland',
                'Pine dominated mixed woodland',
                'Shrubland with sparse tree cover', 'Sugar beet', 'Lucerne', 'Rye',
                'Potatoes', 'Maize', 'Triticale',
                'Other fibre and oleaginous crops', 'Other root crops',
                'Apple fruit', 'Mixed cereals for fodder',
                'Other artificial areas', 'Durum wheat', 'Dry pulses', 'Vineyards',
                'Other bare soil', 'Olive groves', 'Nurseries', 'Oranges',
                'Tomatoes', 'Other leguminous and mixtures for fodder', 'Soya',
                'Other fruit trees and berries', 'Inland fresh running water',
                'Nuts trees', 'Pear fruit', 'Other non-permanent industrial crops',
                'Permanent industrial crops', 'Non built-up linear features',
                'Sand', 'Cotton', 'Cherry fruit', 'Other fresh vegetables',
                'Non built-up area features', 'Lichens and moss',
                'Rocks and stones', 'Other cereals', 'Strawberries',
                'Floriculture and ornamental plants', 'Other citrus fruit', 'Rice',
                'Buildings with 1 to 3 floors', 'Tobacco']
        Returns:
            GeoDataFrame: Contains the groundtruth data within the given region
        """
        if self._groundtruth_points is not None:
            return self._groundtruth_points
        groundtruth_gdf = pd.read_pickle(get_lucas_copernicus_path(),compression='bz2')
        
        if crop is not None:
            groundtruth_gdf = groundtruth_gdf[groundtruth_gdf.LC1_LABEL == crop]
        
        self._groundtruth_points = gpd.sjoin(groundtruth_gdf,self.dataset,predicate="within", how="inner").drop("index_right", axis="columns")
        return self._groundtruth_points


    def get_bbox_with_data(self):
        """

        Returns:
            GeoDataFrame: Contains the bboxes which have associated groundtruth
        """
        if self._bbox_with_groundtruth is not None:
            return self._bbox_with_groundtruth
        self._bbox_with_groundtruth = self.get_groundtruth().copy(deep=True).reset_index()
        self._bbox_with_groundtruth.geometry = self.get_bbox().reset_index().geometry

        return self._bbox_with_groundtruth
    
   
    def get_bbox(self,buffer=0.005,reset=False):

        """
        Creates a grid of bbox over the dataset

        Args:
            dataset (GeoDataFrame): [description]
            expected_bbox_size (int, optional): The desired size of the bbox in meters. Defaults to 2000.
            reset (bool, optional): Wether it should recalculate the bbox_list. Defaults to False.

        Returns:
            GeoDataFrame: GeoDataFrame of the dataset divided in square bbox of size of ´expected_bbox_size´.
        """
        

        # dataset_shape = self.dataset.to_crs("EPSG:3395").geometry.values[-1]

        # width = math.ceil(dataset_shape.bounds[2] - dataset_shape.bounds[0])
        # height = math.ceil(dataset_shape.bounds[3] - dataset_shape.bounds[1])
        # bbox_num_y =  math.ceil(height/expected_bbox_size)
        # bbox_num_x =  math.ceil(width/expected_bbox_size)
        # print(f"bbox_y: {bbox_num_y} bbox_xa:{bbox_num_x}")
        # print(f"width: {width} height: {height}")
        
        #create bboxes around the groundtruth
        if self._dataset_bbox is not None and not reset:
            return self._dataset_bbox
        points_of_interest = [shapely.geometry.MultiPolygon([i.centroid.buffer(0.00001) for i in self.get_groundtruth().geometry.values])]
        points_grid = self.get_groundtruth().geometry.apply(lambda x: BBox(x.centroid.buffer(buffer), sh.CRS.WGS84)).to_list()
        self.dataset_bbox_splitter = CustomGridSplitter(points_of_interest,
            sh.CRS.WGS84.pyproj_crs(),
            points_grid)
        geometry = [Polygon(bbox.get_polygon()) for bbox in self.dataset_bbox_splitter.get_bbox_list()]
        self._dataset_bbox = gpd.GeoDataFrame(crs=sh.CRS.WGS84.pyproj_crs(), geometry=geometry)
        #self._dataset_bbox = self._dataset_bbox.drop_duplicates()
        return self._dataset_bbox
    

    def plot_dataset(self,save_img=None):
        """Plots the existing information in matplotlib

        Args:
            save_img (string, optional): Path to where the plot should be saved. Defaults to None.
        """
        fig, ax = plt.subplots(figsize=(30,30))
        self._dataset_bbox.plot(ax=ax,facecolor='w',edgecolor='r', alpha=0.4)
        self.dataset.plot(ax=ax, facecolor='w', edgecolor='b', alpha=0.5)
        self.get_groundtruth().plot(ax=ax, facecolor='b', alpha=0.5)
        if(self._bbox_with_groundtruth is not None):
            self._bbox_with_groundtruth.plot(ax=ax, facecolor='g', edgecolor='black',alpha=0.7)
        if save_img is not None:
            plt.savefig(os.path.join(save_img,"country.png"))
        else:
            plt.show()


    def _init_classvars(self):
        self.dataset = None
        self._groundtruth_points = None
        self._dataset_bbox = None
        self._bbox_with_groundtruth = None
        self._ground_truth = None        



    def download_images(self,path,bands=None,subset=None,date_field="SURVEY_DATE",date_fmt='%d/%m/%y',padding_days=2,maxcc=0.5):
        """Downloads the specified images into the users filesystem.

        Args:
            path (str): Path to where the dataset should be saved
            subset (DataFrame, optional): Slice of the dataframe returned by `get_bbox_with_data()`.
        """
        if bands is None:
            bands = ["B01","B02","B03","B04","B05","B06","B07","B08","B8A","B09","B11","B12"]
        if subset is None:
            subset = self.get_bbox_with_data()
        if not os.path.isdir(path):
            os.makedirs(path)

        add_data = SentinelHubInputTask(
            bands_feature=(FeatureType.DATA, 'BANDS'),
            resolution=10,
            bands=bands,
            maxcc=maxcc,
            time_difference=datetime.timedelta(minutes=120), 
            data_collection=DataCollection.SENTINEL2_L2A,
            additional_data=[(FeatureType.MASK, 'dataMask'),
                     (FeatureType.MASK, 'CLM'),
                     (FeatureType.DATA, 'CLP')],
            max_threads=5
        )
        add_data_node = EONode(add_data,name="add_data_node")
        
        add_vector = AddFeatureTask((FeatureType.VECTOR_TIMELESS,"LOCATION"))
        add_vector_node = EONode(add_vector,[add_data_node],name="add_vector_node")
        #add_lucas = AddFeature((FeatureType.META_INFO,"LUCAS_DATA"))
        #to get the surrounding data, one can apply a buffered vector to raster and set the non overlapped value some value to distinguish
        add_raster_buffer = VectorToRasterTask((FeatureType.VECTOR_TIMELESS,"LOCATION"),(FeatureType.MASK_TIMELESS,"IS_VALID"), values = 5,buffer=0.0005, raster_shape=(FeatureType.MASK, 'CLM'),no_data_value=0,raster_dtype=np.uint8)
        add_raster_buffer_node = EONode(add_raster_buffer,[add_vector_node],name="add_raster_buffer_node")
        add_raster = VectorToRasterTask((FeatureType.VECTOR_TIMELESS,"LOCATION"),(FeatureType.MASK_TIMELESS,"IS_VALID"), values = 1,raster_shape=(FeatureType.MASK, 'CLM'),write_to_existing = True,no_data_value=0,raster_dtype=np.uint8)
        add_raster_node = EONode(add_raster,[add_raster_buffer_node],name="add_raster_node")
        

        # norm = EuclideanNorm('NORM','BANDS')
        # norm_node = EONode(norm,[add_raster_node],name="norm_node")

        # add_sh_valmask = AddValidDataMaskTask(SentinelHubValidData(),'IS_VALID')
        # add_sh_valmask_node = EONode(add_sh_valmask,[add_raster_node],name="add_sh_valmask_node")
        # add_valid_count = CountValid('IS_VALID', 'VALID_COUNT')
        # add_valid_count_node = EONode(add_valid_count,[add_sh_valmask_node],name="add_valid_count_node")

        concatenate = MergeFeatureTask({FeatureType.DATA: ['BANDS']},(FeatureType.DATA, 'FEATURES'))
        concatenate_node = EONode(concatenate,[add_raster_node],name="concatenate_node")
        save = SaveTask(path, overwrite_permission=OverwritePermission.OVERWRITE_PATCH)
        save_node = EONode(save,[concatenate_node],name="save_node")
        workflow = EOWorkflow([add_data_node,add_vector_node,add_raster_buffer_node,add_raster_node,concatenate_node,save_node])

        execution_args = []
        for id, wrap_bbox in enumerate(subset.iterrows()):
            i, bbox = wrap_bbox

            time_interval = (get_time_interval(bbox[date_field],padding_days,date_fmt=date_fmt))
            gdf = gpd.GeoDataFrame(bbox)
            gdf = gpd.GeoDataFrame(gdf.transpose())
            gdf = gdf.rename(columns={0:'geometry'}).set_geometry('geometry')
            # gdf.set_geometry('geometry')
            gdf.crs = sh.CRS.WGS84.pyproj_crs()

            lucas_points_intersection = self.get_groundtruth()[self.get_groundtruth().geometry.values.intersects(gdf.geometry.values[0])]
            execution_args.append({
                add_vector_node:{'data': lucas_points_intersection},
                add_data_node:{'bbox': BBox(bbox.geometry, crs=self.dataset.crs), 'time_interval': time_interval},
                save_node: {'eopatch_folder': f'eopatch_{id}'}
            })
        executor = EOExecutor(workflow, execution_args, save_logs=True)
        executor.run(workers=5, multiprocess=False)

        executor.make_report()
