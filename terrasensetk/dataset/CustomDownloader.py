from . import Downloader

class CustomDownloader(Downloader):

    def __init__(self,shapefile):
        super().__init__(shapefile=shapefile)

    def get_groundtruth(self, crop=None):
        return self.dataset