//VERSION=3
function setup() {
    return {
        input: ["B01", "B02", "B03", "B04", "B05", "B07"],
        output: { bands: 6 }
    };
}

// Define the evaluatePixel function
function evaluatePixel(sample) {
    return [
        sample.B01,
        sample.B02,
        sample.B03,
        sample.B04,
        sample.B05,
        sample.B07,
    ];
}